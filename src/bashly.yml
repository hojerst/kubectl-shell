name: kubectl-shell
help: Spawn a shell in Kubernetes (in existing or new pod or in node shell)
import: .version.yml

flags:
  - long: --volume
    short: -v
    help: attach a volume (pvc or hostpath) to the pod
    repeatable: true
    arg: mountspec
  - long: --image
    short: -i
    help: image to use for the pod
    arg: image
    default: $KUBECTL_SHELL_IMAGE
  - long: --timeout
    short: -t
    help: timeout for the pod to start (in seconds)
    arg: timeout
    validate: duration
    default: "5m"
  - long: --rm
    help: remove the pod after it exits
    arg: rm
    validate: bool
    allowed: ["true", "false"]
    default: "true"
  - long: --namespace
    short: -n
    help: If present, the namespace scope for this CLI request
    arg: namespace
  - long: --workdir
    short: -w
    help: change working directory in the pod
    arg: workdir
  - long: --privileged
    help: run the pod in privileged mode
    validate: bool
    allowed: ["true", "false"]
    default: "false"
    arg: privileged
  - long: --uid
    short: -u
    help: run with specific user id
    validate: integer
    arg: uid
  - long: --gid
    short: -g
    help: run with specific group id
    validate: integer
    arg: gid
  - long: --service-account
    short: -s
    help: run with specific service account
    default: default
    arg: service-account
  - long: --node
    short: -N
    help: schedule pod on specific node
    arg: node-name
  - long: --image-pull-policy
    help: schedule pod on specific node
    arg: image-pull-policy
    validate: image_pull_policy

environment_variables:
  - name: KUBECTL_SHELL_IMAGE
    help: Default image to spawn
    default: alpine:latest

examples:
  - kubectl shell
  - kubectl shell -v host/home/user:/home/user:ro -v pvc/my-pvc:/mnt:ro
  - kubectl shell -i alpine:latest
