extra_args=""
if [[ "${args[--namespace]+x}" == x ]] ; then
  extra_args="$extra_args --namespace=${args[--namespace]}"
fi

pod_spec=$(generate_pod_spec)
pod_name=$(echo "$pod_spec" | kubectl create -f - -o name $extra_args)

if [[ "${args[--rm]}" == "true" ]] ; then
  trap 'kubectl delete "$pod_name" $extra_args' EXIT
fi

kubectl wait --for=condition=Ready --timeout="${args[--timeout]}" $extra_args "$pod_name"
kubectl attach -it  $extra_args "$pod_name"
