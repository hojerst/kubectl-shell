validate_bool() {
  [[ "$1" =~ ^true|false$ ]] || echo "must be a boolean (true or false)"
}
