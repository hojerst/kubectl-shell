validate_duration() {
  [[ "$1" =~ ^[0-9]+[smyd]$ ]] || echo "must be a duration"
}
