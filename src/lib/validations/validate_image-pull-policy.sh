validate_image_pull_policy() {
  [[ "$1" =~ ^IfNotPresent|Always|Never$ ]] || echo "must be one of IfNotPresent, Always, or Never"
}
