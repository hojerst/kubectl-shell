generate_pod_spec() {
  local volumes=()
  local i

  if [[ ${args[--volume]+x} == x ]]; then
    eval "volumes=(${args[--volume]})"
  fi

  cat <<-EOF
apiVersion: v1
kind: Pod
metadata:
  generateName: kubectl-shell-
spec:
  serviceAccountName: ${args[--service-account]}
  restartPolicy: Never
  containers:
    - name: main
      image: ${args[--image]}
      stdin: true
      stdinOnce: true
      tty: true
EOF

  if [[ ${args[--image-pull-policy]+x} == x ]]; then
    echo "      imagePullPolicy: ${args[--image-pull-policy]}"
  fi

cat <<-EOF
      securityContext:
        privileged: ${args[--privileged]}
EOF

  if [[ ${args[--uid]+x} == x ]]; then
    echo "        runAsUser: ${args[--uid]}"
  fi
  if [[ ${args[--gid]+x} == x ]]; then
    echo "        runAsGroup: ${args[--gid]}"
  fi

  if [[ ${args[--workdir]+x} == x ]]; then
    echo "      workingDir: ${args[--workdir]}"
  fi

  if (( ${#volumes[@]} )); then
    echo "      volumeMounts:"

    i=0
    for volume in ${volumes[@]}; do
      local path="$(echo "$volume" | cut -d: -f2)"
      local flags="$(echo "$volume" | cut -d: -f3)"
      echo "        - name: volume$i"
      echo "          mountPath: $path"
      if [[ "$flags" == "ro" ]]; then
        echo "          readOnly: true"
      fi

      i=$((i+1))
    done

    echo "  volumes:"

    i=0
    for volume in ${volumes[@]}; do
      local source="$(echo "$volume" | cut -d: -f1)"
      echo "    - name: volume$i"

      local type="$(echo "$source" | cut -d/ -f1)"
      local path="$(echo "$source" | sed -e 's#^[^/]*/##')"
      if [ "$type" = "host" ]; then
        echo "      hostPath:"
        echo "        path: /$path"
      elif [ "$type" = "pvc" ]; then
        echo "      persistentVolumeClaim:"
        echo "        claimName: $path"
      elif [ "$type" = "cm" ]; then
        echo "      configMap:"
        echo "        name: $path"
      elif [ "$type" = "secret" ]; then
        echo "      secret:"
        echo "        secretName: $path"
      elif [ "$type" = "empty" ]; then
        echo "      emptyDir:"
      else
        echo 1>&2 "error: unknown volume type: $type"
        exit 1
      fi
      i=$((i+1))
    done
  fi

  if [[ ${args[--node]+x} == x ]]; then
    echo "  nodeSelector:"
    echo "    kubernetes.io/hostname: ${args[--node]}"
  fi
}
