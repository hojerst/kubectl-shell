# kubectl-shell

A small kubectl plugin which allows to spawn shells in kubernetes clusters. Unlike `kubectl run` it allows to easily
attach volumes for inspection.

* [Project website]
* [Source Code]
* [Releases]

## Prerequisites

* [kubectl]
* [bash]: (version 4) - note: MacOS ships with bash v3 and can be updated e.g. with homebrew

## Installation

### Krew

```bash
kubectl krew index add hojerst https://gitlab.com/hojerst/krew-plugins.git
kubectl krew install hojerst/shell
```

### Manual

* Install the prerequisites
* Download `kubectl-shell` from the [Releases] page
* Install `kubectl-shell` to your `$PATH`

## Usage

[![asciicast](https://asciinema.org/a/509008.svg)](https://asciinema.org/a/509008)

```bash
# spawn a shell and connect to it
kubectl shell

# spawn a shell and connect to it AND
#    mount host path `/home/user` as `/home/user` in the pod AND
#    mount pvc `my-pvc` as `/mnt` in read-only mode. 
kubectl shell -v host/home/user:/home/user -v pvc/my-pvc:/mnt:ro

# run shell on a specific node (short form: -N)
kubectl shell --node my-node

# Use a different image and namespace
kubectl shell -i ubuntu:latest -n my-namespace

# change workdir /etc
kubectl shell -w /etc

# run privileged container
kubectl shell --privileged=true

# run with specific uid and gid
kubectl shell --uid=1000 --gid=1000

# run with specific service account (must already exist)
kubectl shell --service-account=my-serviceaccount

# set default shell image through an environment variable
export KUBECTL_SHELL_IMAGE=ubuntu:latest
kubectl shell
```

[bash]: https://www.gnu.org/software/bash/

[kubectl]: https://kubernetes.io/docs/tasks/tools/

[Project website]: https://hojerst.gitlab.io/kubectl-shell/

[Source Code]: https://gitlab.com/hojerst/kubectl-shell/

[Releases]: https://gitlab.com/hojerst/kubectl-shell/-/releases 
